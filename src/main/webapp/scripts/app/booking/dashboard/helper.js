'use strict';

/**
 * @ngdoc service
 * @name facilityApp.helper
 * @description
 * # helper
 * Factory in the facilityApp.
 */

angular
  .module('spaceboostApp')
  .factory('alert', function($uibModal) {

    function show(action, event) {
      return $uibModal.open({
        templateUrl: 'scripts/app/booking/dashboard/modalcontent.html',
        controller: function($scope) {
          var vm = this;
          vm.action = action;
          vm.event = event;
            
        },
        controllerAs: 'vm'
      });
    }

    return {
      show: show
    };

  });
