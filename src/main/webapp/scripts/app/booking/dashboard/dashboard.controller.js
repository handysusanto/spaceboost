/**
 * Created by IT Progremmer2 on 11/04/2016.
 */
'use strict';

/**
 * @ngdoc function
 * @name facilityApp.controller:BookingCtrl
 * @description
 * # BookingCtrl
 * Controller of the facilityApp
 */

angular.module('spaceboostApp')
    .controller('DashboardController', function($scope, $mdDialog, $mdMedia, alert, Principal, Booking, BookingApproved,ParseLinks) {

        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

            

        $scope.status = '';

        $scope.bookings = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            BookingApproved.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                  $scope.totalItems = headers('X-Total-Count');
                $scope.bookings = result;
            });
        };



        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.booking = {
                title: null,
                startsAt: null,
                endsAt: null,
                numberAtt: null,
                nameAtt: null,
                status: false,
                recursOn: null,
                note: null,
                booking_date: new Date(),
                id: null
            };
        };

        $scope.calendarView = 'month';
        $scope.calendarDate = new Date();


        $scope.isCellOpen = true;

        $scope.eventClicked = function(event) {
            alert.show('Booking Detail', event);
        };

        $scope.toggle = function($event, field, event) {
            $event.preventDefault();
            $event.stopPropagation();
            event[field] = !event[field];
        };

        });
    });
