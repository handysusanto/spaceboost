'use strict';

angular.module('spaceboostApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                parent: 'dashboard',
                url: '/home',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                    pageTitle: 'Booking Calendar'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/booking/dashboard/dashboard.html',
                        controller: 'DashboardController'
                    }
                },
                resolve: {

                }
            }) .state('dashboard.edit', {
                parent: 'home',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/booking/booking-edit-dialog.html',
                        controller: 'BookingEditDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Booking', function(Booking) {
                                return Booking.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('home', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('dashboard.new', {
                parent: 'home',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/booking/booking-dialog.html',
                        controller: 'BookingDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    title: null,
                                    startsAt: null,
                                    endsAt: null,
                                    numberAtt: null,
                                    nameAtt: null,
                                    status: false,
                                    recursOn: null,
                                    note: null,
                                    booking_date: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('home', null, { reload: true });
                    }, function() {
                        $state.go('home');
                    })
                }]
            })
            .state('dashboard.delete', {
            parent: 'home',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'scripts/app/entities/booking/booking-delete-dialog.html',
                    controller: 'BookingDeleteController',
                    size: 'md',
                    resolve: {
                        entity: ['Booking', function(Booking) {
                            return Booking.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function(result) {
                    $state.go('home', null, { reload: true });
                }, function() {
                    $state.go('^');
                })
            }]
        });
    });
