'use strict';

angular.module('spaceboostApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('password', {
                parent: 'account',
                url: '/password',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                    pageTitle: 'Password'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/password/password.html',
                        controller: 'PasswordController'
                    }
                },
                resolve: {

                }
            });
    });
