/**
 * Created by IT Progremmer2 on 11/04/2016.
 */
'use strict';

angular.module('spaceboostApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('myprofile', {
                abstract: true,
                parent: 'site'
            });
    });
