/**
 * Created by IT Progremmer2 on 11/04/2016.
 */
'use strict';

angular.module('spaceboostApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('profile', {
                parent: 'myprofile',
                url: '/profile',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                    pageTitle: 'My Profile'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/profile/myprofile/profile.html',
                        controller: 'ProfileCtrl'
                    }
                },
                resolve: {

                }
            });
    });
