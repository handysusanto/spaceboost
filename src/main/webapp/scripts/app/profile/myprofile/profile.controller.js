/**
 * Created by IT Progremmer2 on 12/04/2016.
 */
'use strict';

/**
 * @ngdoc function
 * @name facilityApp.controller:UserRoleCtrl
 * @description
 * # UserRoleCtrl
 * Controller of the facilityApp
 */
angular.module('spaceboostApp')
    .controller('ProfileCtrl', function ($scope, $mdMedia, $mdDialog, Principal) {

        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;
        });



    });

