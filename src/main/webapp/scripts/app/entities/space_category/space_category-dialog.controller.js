'use strict';

angular.module('spaceboostApp').controller('Space_categoryDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Space_category', 'Principal',
        function($scope, $stateParams, $uibModalInstance, entity, Space_category, Principal) {

            Principal.identity().then(function (account) {
                $scope.account = account;
                $scope.isAuthenticated = Principal.isAuthenticated;
                $scope.authority = account.authorities;



                $scope.space_category = entity;
        $scope.load = function(id) {
            Space_category.get({id : id}, function(result) {
                $scope.space_category = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('spaceboostApp:space_categoryUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            if($scope.authority[1] == "ROLE_ADMIN") {
                $scope.isSaving = true;
                if ($scope.space_category.id != null) {
                    Space_category.update($scope.space_category, onSaveSuccess, onSaveError);
                } else {
                    Space_category.save($scope.space_category, onSaveSuccess, onSaveError);
                }
            }
            else{
                $scope.isSaving = false;
                $uibModalInstance.close();
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
            });
}]);
