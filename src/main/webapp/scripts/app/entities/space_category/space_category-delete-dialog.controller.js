'use strict';

angular.module('spaceboostApp')
	.controller('Space_categoryDeleteController', function($scope, $uibModalInstance, entity, Space_category) {

        $scope.space_category = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Space_category.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
