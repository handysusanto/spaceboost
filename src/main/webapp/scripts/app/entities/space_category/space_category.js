'use strict';

angular.module('spaceboostApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('space_category', {
                parent: 'entity',
                url: '/space_categorys',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                    pageTitle: 'Space Category'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/space_category/space_categorys.html',
                        controller: 'Space_categoryController'
                    }
                },
                resolve: {
                }
            })
            .state('space_category.detail', {
                parent: 'entity',
                url: '/space_category/{id}',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                    pageTitle: 'Space_category'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/space_category/space_category-detail.html',
                        controller: 'Space_categoryDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Space_category', function($stateParams, Space_category) {
                        return Space_category.get({id : $stateParams.id});
                    }]
                }
            })
            .state('space_category.new', {
                parent: 'space_category',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/space_category/space_category-dialog.html',
                        controller: 'Space_categoryDialogController',
                        size: 'sm',
                        resolve: {
                            entity: function () {
                                return {
                                    space_category: null,
                                    description: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('space_category', null, { reload: true });
                    }, function() {
                        $state.go('space_category');
                    })
                }]
            })
            .state('space_category.edit', {
                parent: 'space_category',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/space_category/space_category-dialog.html',
                        controller: 'Space_categoryDialogController',
                        size: 'sm',
                        resolve: {
                            entity: ['Space_category', function(Space_category) {
                                return Space_category.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('space_category', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('space_category.delete', {
                parent: 'space_category',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/space_category/space_category-delete-dialog.html',
                        controller: 'Space_categoryDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Space_category', function(Space_category) {
                                return Space_category.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('space_category', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
