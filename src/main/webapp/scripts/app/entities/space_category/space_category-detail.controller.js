'use strict';

angular.module('spaceboostApp')
    .controller('Space_categoryDetailController', function ($scope, $rootScope, $stateParams, entity, Space_category) {
        $scope.space_category = entity;
        $scope.load = function (id) {
            Space_category.get({id: id}, function(result) {
                $scope.space_category = result;
            });
        };
        var unsubscribe = $rootScope.$on('spaceboostApp:space_categoryUpdate', function(event, result) {
            $scope.space_category = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
