'use strict';

angular.module('spaceboostApp')
    .controller('Space_categoryController', function ($scope, $state, Space_category, ParseLinks, Principal) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

                $scope.chkAuth = function () {

                    if ($scope.account.authorities[0] == 'ROLE_ADMIN') {
                        return true;
                    }
                    else if ($scope.account.authorities[0] == 'ROLE_ROLE' || $scope.account.authorities[1] == 'ROLE_ADMIN') {
                        return true;
                    }
                    else {
                        return false;
                    }
                };
        });
        $scope.space_categorys = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;

        $scope.loadAll = function() {
            Space_category.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.space_categorys = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };

        $scope.loadAll();

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.space_category = {
                space_category: null,
                description: null,
                id: null
            };
        };
    });
