'use strict';

angular.module('spaceboostApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('space', {
                parent: 'entity',
                url: '/spaces',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                    pageTitle: 'Spaces'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/space/spaces.html',
                        controller: 'SpaceController'
                    }
                },
                resolve: {
                }
            })
            .state('space.detail', {
                parent: 'entity',
                url: '/space/{id}',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                    pageTitle: 'Space'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/space/space-detail.html',
                        controller: 'SpaceDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Space', function($stateParams, Space) {
                        return Space.get({id : $stateParams.id});
                    }]
                }
            })
            .state('space.new', {
                parent: 'space',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/space/space-dialog.html',
                        controller: 'SpaceDialogController',
                        size: 'md',
                        resolve: {
                            entity: function () {
                                return {
                                    name_space: null,
                                    location: null,
                                    capacity: null,
                                    facility_space: null,
                                    note_space: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('space', null, { reload: true });
                    }, function() {
                        $state.go('space');
                    })
                }]
            })
            .state('space.edit', {
                parent: 'space',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/space/space-dialog.html',
                        controller: 'SpaceDialogController',
                        size: 'md',
                        resolve: {
                            entity: ['Space', function(Space) {
                                return Space.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('space', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('space.delete', {
                parent: 'space',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/space/space-delete-dialog.html',
                        controller: 'SpaceDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Space', function(Space) {
                                return Space.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('space', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
