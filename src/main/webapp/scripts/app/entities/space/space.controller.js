'use strict';

angular.module('spaceboostApp')
    .controller('SpaceController', function ($scope, Principal, Auth, $state, Space, SpaceReady, ParseLinks) {

        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

                $scope.chkAuth = function () {

                    if($scope.account.authorities[0] == 'ROLE_ADMIN'){
                        return true;
                    }
                    else if($scope.account.authorities[0] == 'ROLE_USER' || $scope.account.authorities[1] == 'ROLE_ADMIN'){
                        return true;
                    }
                    else{
                        return false;
                    }
            };


        });
        $scope.spaces = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function() {
            Space.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.spaces = result;
                console.log($scope.spaces);
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };

        $scope.loadAll();

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.space = {
                name_space: null,
                location: null,
                capacity: null,
                facility_space: null,
                note_space: null,
                id: null
            };
        };
    });
