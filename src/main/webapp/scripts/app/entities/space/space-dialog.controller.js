'use strict';

angular.module('spaceboostApp').controller('SpaceDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Space', 'Space_category', 'Principal',
        function($scope, $stateParams, $uibModalInstance, entity, Space, Space_category, Principal) {

            Principal.identity().then(function (account) {
                $scope.account = account;
                $scope.isAuthenticated = Principal.isAuthenticated;
                $scope.authority = account.authorities;


                $scope.space = entity;
        $scope.space_categorys = Space_category.query();
        $scope.load = function(id) {
            Space.get({id : id}, function(result) {
                $scope.space = result;
            });
        };


        var onSaveSuccess = function (result) {
            $scope.$emit('spaceboostApp:spaceUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            if($scope.authority[1] == "ROLE_ADMIN") {
                $scope.isSaving = true;
                if ($scope.space.id != null) {
                    Space.update($scope.space, onSaveSuccess, onSaveError);
                } else {
                    Space.save($scope.space, onSaveSuccess, onSaveError);
                }
            }
            else{
                $scope.isSaving = false;
                $uibModalInstance.close();
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

            });
}]);
