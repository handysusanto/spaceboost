'use strict';

angular.module('spaceboostApp')
    .controller('SpaceDetailController', function ($scope, $rootScope, $stateParams, entity, Space) {
        $scope.space = entity;
        $scope.load = function (id) {
            Space.get({id: id}, function(result) {
                $scope.space = result;
            });
        };
        var unsubscribe = $rootScope.$on('spaceboostApp:spaceUpdate', function(event, result) {
            $scope.space = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
