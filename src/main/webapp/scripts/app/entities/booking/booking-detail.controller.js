'use strict';

angular.module('spaceboostApp')
    .controller('BookingDetailController', function ($scope, $rootScope, $stateParams, entity, Booking) {
        $scope.booking = entity;
        $scope.load = function (id) {
            Booking.get({id: id}, function(result) {
                $scope.booking = result;
            });
        };
        var unsubscribe = $rootScope.$on('spaceboostApp:bookingUpdate', function(event, result) {
            $scope.booking = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
