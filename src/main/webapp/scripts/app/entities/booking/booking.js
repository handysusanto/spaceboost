'use strict';

angular.module('spaceboostApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('booking', {
                parent: 'entity',
                url: '/bookings',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                    pageTitle: 'Booking List'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/booking/bookings.html',
                        controller: 'BookingController'
                    }
                },
                resolve: {
                }
            })
            .state('booking.detail', {
                parent: 'entity',
                url: '/booking/{id}',
                data: {
                    authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_GUEST'],
                    pageTitle: 'Booking'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/booking/booking-detail.html',
                        controller: 'BookingDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Booking', function($stateParams, Booking) {
                        return Booking.get({id : $stateParams.id});
                    }]
                }
            })
            .state('booking.new', {
                parent: 'booking',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/booking/booking-dialog.html',
                        controller: 'BookingDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    title: null,
                                    startsAt: null,
                                    endsAt: null,
                                    numberAtt: null,
                                    nameAtt: null,
                                    status: false,
                                    recursOn: null,
                                    note: null,
                                    booking_date: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('booking', null, { reload: true });
                    }, function() {
                        $state.go('booking');
                    })
                }]
            })
            .state('booking.edit', {
                parent: 'booking',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/booking/booking-edit-dialog.html',
                        controller: 'BookingEditDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Booking', function(Booking) {
                                return Booking.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('booking', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('booking.delete', {
                parent: 'booking',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/booking/booking-delete-dialog.html',
                        controller: 'BookingDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Booking', function(Booking) {
                                return Booking.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('booking', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
