'use strict';

angular.module('spaceboostApp').controller('BookingDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Booking', 'User', 'Space', 'Principal', 'SpaceReady',
        function($scope, $stateParams, $uibModalInstance, entity, Booking, User, Space, Principal, SpaceReady) {

            Principal.identity().then(function (account) {
                $scope.account = account;
                $scope.isAuthenticated = Principal.isAuthenticated;
                $scope.booking = entity;
                $scope.users = User.query();
                console.log($scope.account);
                $scope.spaces = SpaceReady.query();

                $scope.booking.booking_date = new Date();
                $scope.booking.status = "Pending";
                $scope.booking.bookingBy = account.login;
              

                $scope.load = function(id) {
                    Booking.get({id : id}, function(result) {
                        $scope.booking = result.log;
                    });
                };

        var onSaveSuccess = function (result) {
            $scope.$emit('spaceboostApp:bookingUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
                $scope.isSaving = true;
                if ($scope.booking.id != null) {
                    Booking.update($scope.booking, onSaveSuccess, onSaveError);
                } else {
                    Booking.save($scope.booking, onSaveSuccess, onSaveError);
                }

        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForStartsAt = {};

        $scope.datePickerForStartsAt.status = {
            opened: false
        };

        $scope.datePickerForStartsAtOpen = function($event) {
            $scope.datePickerForStartsAt.status.opened = true;
        };

        $scope.datePickerForEndsAt = {};

        $scope.datePickerForEndsAt.status = {
            opened: false
        };

        $scope.datePickerForEndsAtOpen = function($event) {
            $scope.datePickerForEndsAt.status.opened = true;
        };
        $scope.datePickerForBooking_date = new Date();

        $scope.datePickerForBooking_date.status = {
            opened: false
        };

            });
}]);
