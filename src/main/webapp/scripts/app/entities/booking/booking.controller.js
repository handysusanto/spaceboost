'use strict';

angular.module('spaceboostApp')
    .controller('BookingController', function ($scope, $state, Booking, ParseLinks, Auth, Principal,BookingByBookedBy, BookingApproved, BookingRejected, BookingPending ) {

        $scope.isAuthenticated = Principal.isAuthenticated;


            Principal.identity().then(function(account) {
                $scope.account = account;
                $scope.accLogin = account.login;
                $scope.isAuthenticated = Principal.isAuthenticated;


                $scope.chkAuth = function () {
                if(($scope.account.authorities[0] == 'ROLE_USER') && ($scope.account.authorities[1] == 'ROLE_ADMIN')){
                    return false;
                }

                else if($scope.account.authorities[0] == 'ROLE_USER'){
                    return true;
                }

                else{
                    return false;
                }
            };
        });

        $scope.bookings = [];
        $scope.predicate = 'id';
        $scope.reverse = true;
        $scope.page = 1;
        $scope.loadAll = function () {
            Booking.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.bookingss = result;
            });

        };
        
        $scope.loadAllByBooked = function () {
            BookingByBookedBy.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.bookings = result;
            });

        };

        $scope.loadAllApproved = function () {
            BookingApproved.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.approved = result;
            });

        };




        $scope.loadAllRejected = function () {
            BookingRejected.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.bookings = result;
            });

        };

        $scope.loadAllPending = function () {
            BookingPending.query({page: $scope.page - 1, size: 20, sort: [$scope.predicate + ',' + ($scope.reverse ? 'asc' : 'desc'), 'id']}, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.totalItems = headers('X-Total-Count');
                $scope.bookings = result;
            });

        };


        // BookingByBookedBy.getListByBookedBy().then(function (response) {
        //         console.log(response);
        //         $scope.bookings = response.data;
        //     },
        //     function (error) {
        //         console.log(error);
        //     })

        $scope.loadAllByBooked();
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAllByBooked();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.booking = {
                title: null,
                startsAt: null,
                endsAt: null,
                numberAtt: null,
                nameAtt: null,
                status: false,
                recursOn: null,
                note: null,
                booking_date: null,
                id: null
            };
        };


    })
    .controller('modalController', function ($scope, $state, Booking, ParseLinks, Auth, Principal) {
        Principal.identity().then(function(account) {
            $scope.accLogin = account.login;
            $scope.authorities = account.authorities;
            $scope.isAuthenticated = Principal.isAuthenticated;


            $scope.chk = function (booked_by) {
                return ($scope.accLogin == booked_by || $scope.authorities[1]=='ROLE_ADMIN')
            }

        });
    });


