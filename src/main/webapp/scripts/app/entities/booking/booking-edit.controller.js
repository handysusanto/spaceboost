/**
 * Created by IT Progremmer2 on 04/05/2016.
 */
'use strict';

angular.module('spaceboostApp').controller('BookingEditDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Booking', 'User', 'Space' , 'Principal', '$http',
        function($scope, $stateParams, $uibModalInstance, entity, Booking, User, Space, Principal, $http) {

            Principal.identity().then(function (account) {
                $scope.account = account;
                $scope.isAuthenticated = Principal.isAuthenticated;
                $scope.booking = entity;

                $scope.booking.bookedBy = $scope.account;

                $scope.users = User.query();
                $scope.spaces = Space.query();

                $scope.newDate = new Date();


                $scope.authority = account.authorities;

                $scope.accountLogin = account.login;

                $scope.load = function(id) {
                    Booking.get({id : id}, function(result) {
                        $scope.booking = result;
                    });
                };


                $scope.test = function () {
                    return $http.get('api/bookings');
                };

                var onSaveSuccess = function (result) {
                    $scope.$emit('spaceboostApp:bookingUpdate', result);
                    $uibModalInstance.close(result);
                    $scope.isSaving = false;
                };

                var onSaveError = function (result) {
                    $scope.isSaving = false;
                };


                console.log($scope.accountLogin == $scope.booking.bookedBy);
                $scope.save = function () {
                    if($scope.authority[1] != "ROLE_ADMIN") {
                        $scope.booking.status = "Pending";
                        $scope.isSaving = true;
                        if ($scope.booking.id != null) {
                            Booking.update($scope.booking, onSaveSuccess, onSaveError);
                        } else {
                            Booking.save($scope.booking, onSaveSuccess, onSaveError);
                        }
                    }
                    else if($scope.authority[1] == "ROLE_ADMIN"){
                        $scope.isSaving = true;
                        if ($scope.booking.id != null) {
                            Booking.update($scope.booking, onSaveSuccess, onSaveError);
                        } else {
                            Booking.save($scope.booking, onSaveSuccess, onSaveError);
                        }
                    }
                    else{
                        $scope.isSaving = false;
                        $uibModalInstance.close();
                    }

                };

                $scope.clear = function() {
                    $uibModalInstance.dismiss('cancel');
                };
                $scope.datePickerForStartsAt = {};

                $scope.datePickerForStartsAt.status = {
                    opened: false
                };

                $scope.datePickerForStartsAtOpen = function($event) {
                    $scope.datePickerForStartsAt.status.opened = true;
                };

                $scope.datePickerForEndsAt = {};

                $scope.datePickerForEndsAt.status = {
                    opened: false
                };

                $scope.datePickerForEndsAtOpen = function($event) {
                    $scope.datePickerForEndsAt.status.opened = true;
                };
                $scope.datePickerForBooking_date = new Date();

                $scope.datePickerForBooking_date.status = {
                    opened: false
                };

            });
        }]);
