'use strict';

angular.module('spaceboostApp')
    .controller('NavbarController', function ($scope, $location, $state, Auth, $timeout, Principal, ENV) {
        $scope.isAuthenticated = Principal.isAuthenticated;
        $scope.$state = $state;
        $scope.inProduction = ENV === 'prod';



        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.accountLog = account.login;

            $scope.logout = function () {
            Auth.logout();
            $state.go('login');
                $scope.accountLog = account.login;
        };
        });


        $scope.user = {};
        $scope.errors = {};

        $scope.rememberMe = true;
        $timeout(function (){angular.element('[ng-model="username"]').focus();});
        $scope.login = function (event) {
            event.preventDefault();
            Auth.login({
                username: $scope.username,
                password: $scope.password,
                rememberMe: $scope.rememberMe
            }).then(function () {
                $scope.authenticationError = false;
                if ($rootScope.previousStateName === 'register') {
                    $state.go('home');
                } else {
                    $rootScope.back();
                }
            }).catch(function () {
                $scope.authenticationError = true;
            });

        };

    });
