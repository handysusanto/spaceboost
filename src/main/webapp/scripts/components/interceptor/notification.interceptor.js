 'use strict';

angular.module('spaceboostApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-spaceboostApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-spaceboostApp-params')});
                }
                return response;
            }
        };
    });
