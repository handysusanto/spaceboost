'use strict';

angular.module('spaceboostApp')
    .factory('Booking', function ($resource, DateUtils) {
        return $resource('api/bookings/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.startsAt = DateUtils.convertDateTimeFromServer(data.startsAt);
                    data.endsAt = DateUtils.convertDateTimeFromServer(data.endsAt);
                    data.booking_date = DateUtils.convertDateTimeFromServer(data.booking_date);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    })
    .factory('BookingByBookedBy', function ($resource, DateUtils) {
        return $resource('api/bookings/by-login/', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.startsAt = DateUtils.convertDateTimeFromServer(data.startsAt);
                    data.endsAt = DateUtils.convertDateTimeFromServer(data.endsAt);
                    data.booking_date = DateUtils.convertDateTimeFromServer(data.booking_date);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
})  .factory('BookingApproved', function ($resource, DateUtils) {
    return $resource('api/bookings/by-status/approved', {}, {
        'query': { method: 'GET', isArray: true},
        'get': {
            method: 'GET',
            transformResponse: function (data) {
                data.startsAt = DateUtils.convertDateTimeFromServer(data.startsAt);
                data.endsAt = DateUtils.convertDateTimeFromServer(data.endsAt);
                data.booking_date = DateUtils.convertDateTimeFromServer(data.booking_date);
                data = angular.fromJson(data);
                return data;
            }
        },
        'update': { method:'PUT' }
    });
}) .factory('BookingRejected', function ($resource, DateUtils) {
    return $resource('api/bookings/by-status/rejected', {}, {
        'query': { method: 'GET', isArray: true},
        'get': {
            method: 'GET',
            transformResponse: function (data) {
                data.startsAt = DateUtils.convertDateTimeFromServer(data.startsAt);
                data.endsAt = DateUtils.convertDateTimeFromServer(data.endsAt);
                data.booking_date = DateUtils.convertDateTimeFromServer(data.booking_date);
                data = angular.fromJson(data);
                return data;
            }
        },
        'update': { method:'PUT' }
    });
}) .factory('BookingPending', function ($resource, DateUtils) {
        return $resource('api/bookings/by-status/pendings', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data.startsAt = DateUtils.convertDateTimeFromServer(data.startsAt);
                    data.endsAt = DateUtils.convertDateTimeFromServer(data.endsAt);
                    data.booking_date = DateUtils.convertDateTimeFromServer(data.booking_date);
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });

