package com.nebula.spaceboost.repository;

import com.nebula.spaceboost.domain.Space;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Space entity.
 */
public interface SpaceRepository extends JpaRepository<Space,Long> {

    Page<Space> findAllByStatusSpace(String status_space, Pageable pageable);

}
