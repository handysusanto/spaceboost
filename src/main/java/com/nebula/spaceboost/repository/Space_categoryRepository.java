package com.nebula.spaceboost.repository;

import com.nebula.spaceboost.domain.Space_category;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Space_category entity.
 */
public interface Space_categoryRepository extends JpaRepository<Space_category,Long> {

}
