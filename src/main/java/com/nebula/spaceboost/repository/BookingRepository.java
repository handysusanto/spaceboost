package com.nebula.spaceboost.repository;

import com.nebula.spaceboost.domain.Booking;
import com.nebula.spaceboost.domain.Space;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data JPA repository for the Booking entity.
 */
public interface BookingRepository extends JpaRepository<Booking,Long> {


    Page<Booking> findAllByBookingBy(String bookingBy, Pageable pageable);

    //cari booking status id
    Page<Booking> findAllByStatus(String status, Pageable pageable);


    List<Booking> findByStartsAtBetweenAndSpaceAndStatus(ZonedDateTime startsAt, ZonedDateTime endsAt, Space space, String status);
    List<Booking> findByStartsAtLessThanEqualAndEndsAtGreaterThanEqualAndSpaceAndStatus(ZonedDateTime startsAt, ZonedDateTime endsAt, Space space, String status);
    List<Booking> findByEndsAtBetweenAndSpaceAndStatus(ZonedDateTime startsAt, ZonedDateTime endsAt, Space space, String status);


}
