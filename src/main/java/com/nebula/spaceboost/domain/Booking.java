package com.nebula.spaceboost.domain;

import java.time.ZonedDateTime;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Booking.
 */
@Entity
@Table(name = "booking")
public class Booking implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "starts_at", nullable = false)
    private ZonedDateTime startsAt;

    @NotNull
    @Column(name = "ends_at", nullable = false)
    private ZonedDateTime endsAt;

    @NotNull
    @Column(name = "number_att", nullable = false)
    private Integer numberAtt;

    @NotNull
    @Column(name = "name_att", nullable = false)
    private String nameAtt;

    @Column(name = "recurs_on")
    private String recursOn;

    @Column(name = "note")
    private String note;


    @Column(name = "booking_date")
    private ZonedDateTime booking_date;

    @Column(name = "status", nullable = false)
    private String status;

    @NotNull
    @Column(name = "bookingBy", nullable = false)
    private String bookingBy;

    @ManyToOne
    @JoinColumn(name = "on_space_id")
    private Space space;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ZonedDateTime getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(ZonedDateTime startsAt) {
        this.startsAt = startsAt;
    }

    public ZonedDateTime getEndsAt() {
        return endsAt;
    }

    public void setEndsAt(ZonedDateTime endsAt) {
        this.endsAt = endsAt;
    }

    public Integer getNumberAtt() {
        return numberAtt;
    }

    public void setNumberAtt(Integer numberAtt) {
        this.numberAtt = numberAtt;
    }

    public String getNameAtt() {
        return nameAtt;
    }

    public void setNameAtt(String nameAtt) {
        this.nameAtt = nameAtt;
    }

    public String getRecursOn() {
        return recursOn;
    }

    public void setRecursOn(String recursOn) {
        this.recursOn = recursOn;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public ZonedDateTime getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(ZonedDateTime booking_date) {
        this.booking_date = booking_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBookingBy() {
        return bookingBy;
    }

    public void setBookingBy(String bookingBy) {
        this.bookingBy = bookingBy;
    }


    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Booking booking = (Booking) o;
        if(booking.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, booking.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Booking{" +
            "id=" + id +
            ", title='" + title + "'" +
            ", startsAt='" + startsAt + "'" +
            ", endsAt='" + endsAt + "'" +
            ", numberAtt='" + numberAtt + "'" +
            ", nameAtt='" + nameAtt + "'" +
            ", recursOn='" + recursOn + "'" +
            ", note='" + note + "'" +
            ", booking_date='" + booking_date + "'" +
            '}';
    }
}
