package com.nebula.spaceboost.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Space_category.
 */
@Entity
@Table(name = "space_category")
public class Space_category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "space_category", nullable = false)
    private String space_category;
    
    @NotNull
    @Column(name = "description", nullable = false)
    private String description;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpace_category() {
        return space_category;
    }
    
    public void setSpace_category(String space_category) {
        this.space_category = space_category;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Space_category space_category = (Space_category) o;
        if(space_category.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, space_category.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Space_category{" +
            "id=" + id +
            ", space_category='" + space_category + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
