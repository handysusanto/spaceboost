package com.nebula.spaceboost.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Space.
 */
@Entity
@Table(name = "space")
public class Space implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name_space", nullable = false)
    private String name_space;

    @NotNull
    @Column(name = "location", nullable = false)
    private String location;

    @NotNull
    @Column(name = "capacity", nullable = false)
    private Integer capacity;

    @NotNull
    @Column(name = "facility_space", nullable = false)
    private String facility_space;

    @Column(name = "note_space")
    private String note_space;


    @Column(name = "status_space")
    private String statusSpace;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Space_category category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName_space() {
        return name_space;
    }

    public void setName_space(String name_space) {
        this.name_space = name_space;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getFacility_space() {
        return facility_space;
    }

    public void setFacility_space(String facility_space) {
        this.facility_space = facility_space;
    }

    public String getNote_space() {
        return note_space;
    }


    public String getStatusSpace() {
        return statusSpace;
    }

    public void setStatusSpace(String statusSpace) {
        this.statusSpace = statusSpace;
    }

    public void setNote_space(String note_space) {
        this.note_space = note_space;
    }



    public Space_category getCategory() {
        return category;
    }

    public void setCategory(Space_category space_category) {
        this.category = space_category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Space space = (Space) o;
        if(space.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, space.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Space{" +
            "id=" + id +
            ", name_space='" + name_space + "'" +
            ", location='" + location + "'" +
            ", capacity='" + capacity + "'" +
            ", facility_space='" + facility_space + "'" +
            ", note_space='" + note_space + "'" +
            '}';
    }
}
