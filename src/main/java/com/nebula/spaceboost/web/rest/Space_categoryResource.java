package com.nebula.spaceboost.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nebula.spaceboost.domain.Space_category;
import com.nebula.spaceboost.repository.Space_categoryRepository;
import com.nebula.spaceboost.web.rest.util.HeaderUtil;
import com.nebula.spaceboost.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Space_category.
 */
@RestController
@RequestMapping("/api")
public class Space_categoryResource {

    private final Logger log = LoggerFactory.getLogger(Space_categoryResource.class);

    @Inject
    private Space_categoryRepository space_categoryRepository;

    /**
     * POST  /space_categorys -> Create a new space_category.
     */
    @RequestMapping(value = "/space_categorys",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Space_category> createSpace_category(@Valid @RequestBody Space_category space_category) throws URISyntaxException {
        log.debug("REST request to save Space_category : {}", space_category);
        if (space_category.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("space_category", "idexists", "A new space_category cannot already have an ID")).body(null);
        }
        Space_category result = space_categoryRepository.save(space_category);
        return ResponseEntity.created(new URI("/api/space_categorys/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("space_category", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /space_categorys -> Updates an existing space_category.
     */
    @RequestMapping(value = "/space_categorys",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Space_category> updateSpace_category(@Valid @RequestBody Space_category space_category) throws URISyntaxException {
        log.debug("REST request to update Space_category : {}", space_category);
        if (space_category.getId() == null) {
            return createSpace_category(space_category);
        }
        Space_category result = space_categoryRepository.save(space_category);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("space_category", space_category.getId().toString()))
            .body(result);
    }

    /**
     * GET  /space_categorys -> get all the space_categorys.
     */
    @RequestMapping(value = "/space_categorys",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Space_category>> getAllSpace_categorys(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Space_categorys");
        Page<Space_category> page = space_categoryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/space_categorys");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /space_categorys/:id -> get the "id" space_category.
     */
    @RequestMapping(value = "/space_categorys/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Space_category> getSpace_category(@PathVariable Long id) {
        log.debug("REST request to get Space_category : {}", id);
        Space_category space_category = space_categoryRepository.findOne(id);
        return Optional.ofNullable(space_category)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /space_categorys/:id -> delete the "id" space_category.
     */
    @RequestMapping(value = "/space_categorys/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSpace_category(@PathVariable Long id) {
        log.debug("REST request to delete Space_category : {}", id);
        space_categoryRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("space_category", id.toString())).build();
    }
}
