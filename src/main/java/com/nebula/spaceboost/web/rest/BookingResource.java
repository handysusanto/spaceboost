package com.nebula.spaceboost.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.nebula.spaceboost.domain.Booking;
import com.nebula.spaceboost.domain.Space;
import com.nebula.spaceboost.repository.BookingRepository;
import com.nebula.spaceboost.security.SecurityUtils;
import com.nebula.spaceboost.web.rest.util.HeaderUtil;
import com.nebula.spaceboost.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller for managing Booking.
 */
@RestController
@RequestMapping("/api")
public class BookingResource {

    private final Logger log = LoggerFactory.getLogger(BookingResource.class);


    @Inject
    private BookingRepository bookingRepository;

    /**
     * POST  /bookings -> Create a new booking.
     */
    @RequestMapping(value = "/bookings",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Booking> createBooking(@Valid @RequestBody Booking booking) throws URISyntaxException {
        log.debug("REST request to save Booking : {}", booking);

        if (booking.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "A new booking cannot already have an ID")).body(null);
        }

        if(booking.getStartsAt().getYear() == booking.getEndsAt().getYear()) {
            if(booking.getStartsAt().getMonthValue() == booking.getEndsAt().getMonthValue()) {
                if(booking.getStartsAt().getHour() >=1 && booking.getStartsAt().getHour() <= 13 && booking.getEndsAt().getHour()>=1 && booking.getEndsAt().getHour() <= 13) {
                    if(booking.getStartsAt().getDayOfMonth() == booking.getEndsAt().getDayOfMonth()) {
                        if(booking.getStartsAt().getHour() < booking.getEndsAt().getHour()) {
                            if((booking.getEndsAt().getHour()-booking.getStartsAt().getHour()) == 1) {
                                if((booking.getEndsAt().getMinute()-booking.getStartsAt().getMinute())>=0) {
                                        if(booking.getNumberAtt() <= booking.getSpace().getCapacity()) {
                                            if (booking.getTitle().length() >= 3) {
                                                Booking result = bookingRepository.save(booking);
                                                return ResponseEntity.ok()
                                                    .headers(HeaderUtil.createEntityUpdateAlert("booking", booking.getId().toString()))
                                                    .body(result);
                                            }

                                            else{
                                                return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists",  "Title Minimal 3 Character")).body(null);
                                            }
                                        }
                                        else{
                                            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Ruang: " + booking.getSpace().getName_space() + " " + "Max Capacity: " + booking.getSpace().getCapacity())).body(null);
                                        }

                                }
                                else{
                                    return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Booking Minimal 1 Hour")).body(null);
                                }
                            }
                            else{
                                    if(booking.getNumberAtt() <= booking.getSpace().getCapacity()) {
                                        Booking result = bookingRepository.save(booking);
                                        return ResponseEntity.ok()
                                            .headers(HeaderUtil.createEntityUpdateAlert("booking", booking.getId().toString()))
                                            .body(result);
                                    }
                                    else {
                                        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Ruang: " + booking.getSpace().getName_space() + " " + "Max Capacity: " + booking.getSpace().getCapacity())).body(null);
                                    }

                            }
                        }
                        else if( booking.getStartsAt().getHour() ==  booking.getEndsAt().getHour()){
                            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Booking Minimal 1 Hour")).body(null);
                        }
                        else{
                            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Invalid TIme")).body(null);
                        }
                    }
                    else {
                        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Invalid Date")).body(null);
                    }
                }
                else{
                    return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Booking time minimal 08:00 AM until 08:00 PM")).body(null);
                }
            }
            else {
                return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Invalid Date")).body(null);
            }
        }else {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Invalid Date")).body(null);
        }

    }

    /**
     * PUT  /bookings -> Updates an existing booking.
     *
     */
    @RequestMapping(value = "/bookings",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Booking> updateBooking(@Valid @RequestBody Booking booking, Space space) throws URISyntaxException {
        log.debug("REST request to update Booking : {}", booking);
        if (booking.getId() == null) {
            return createBooking(booking);
        }

        List<Booking> results = bookingRepository.findByStartsAtBetweenAndSpaceAndStatus(booking.getStartsAt(), booking.getEndsAt(), booking.getSpace(), "Approved");
        List<Booking> results2 = bookingRepository.findByEndsAtBetweenAndSpaceAndStatus(booking.getStartsAt(), booking.getEndsAt(), booking.getSpace(), "Approved");
        List<Booking> results3 = bookingRepository.findByStartsAtLessThanEqualAndEndsAtGreaterThanEqualAndSpaceAndStatus(booking.getStartsAt(), booking.getEndsAt(), booking.getSpace(), "Approved");


        int Result = results.size();
        int Result2 = results2.size();
        int Result3 = results3.size();

        if(booking.getStartsAt().getYear() == booking.getEndsAt().getYear()) {
            if(booking.getStartsAt().getMonthValue() == booking.getEndsAt().getMonthValue()) {
                if(booking.getStartsAt().getHour() >=1 && booking.getStartsAt().getHour() <= 13 && booking.getEndsAt().getHour()>=1 && booking.getEndsAt().getHour() <= 13) {
                    if(booking.getStartsAt().getDayOfMonth() == booking.getEndsAt().getDayOfMonth()) {
                        if(booking.getStartsAt().getHour() < booking.getEndsAt().getHour()) {
                            if((booking.getEndsAt().getHour()-booking.getStartsAt().getHour()) == 1) {
                                if((booking.getEndsAt().getMinute()-booking.getStartsAt().getMinute())>=0) {
                                    if ((((Result == 1) && (results.get(0).getId() == booking.getId())) || (Result == 0)) && (((Result2 == 1) && (results2.get(0).getId() == booking.getId())) || (Result2 == 0)) && (((Result3 == 1) && (results3.get(0).getId() == booking.getId())) || Result3 == 0) || booking.getStatus().equals("Rejected"))
                                    {
                                        if(booking.getNumberAtt() <= booking.getSpace().getCapacity()) {
                                            if (booking.getTitle().length() >= 3) {
                                                Booking result = bookingRepository.save(booking);
                                                return ResponseEntity.ok()
                                                    .headers(HeaderUtil.createEntityUpdateAlert("booking", booking.getId().toString()))
                                                    .body(result);
                                            }

                                            else{
                                                return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists",  "Title Minimal 3 Character")).body(null);
                                            }
                                        }
                                        else{
                                            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Ruang: " + booking.getSpace().getName_space() + " " + "Max Capacity: " + booking.getSpace().getCapacity())).body(null);
                                        }
                                    } else {

                                        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Booking already exist")).body(null);
                                    }
                                }
                                else{
                                    return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Booking Minimal 1 Hour")).body(null);
                                }
                            }
                            else{
                                if ((((Result == 1) && (results.get(0).getId() == booking.getId())) || (Result == 0)) && (((Result2 == 1) && (results2.get(0).getId() == booking.getId())) || (Result2 == 0)) && (((Result3 == 1) && (results3.get(0).getId() == booking.getId())) || Result3 == 0) || booking.getStatus().equals("Rejected"))
                                {
                                    if(booking.getNumberAtt() <= booking.getSpace().getCapacity()) {
                                        Booking result = bookingRepository.save(booking);
                                        return ResponseEntity.ok()
                                            .headers(HeaderUtil.createEntityUpdateAlert("booking", booking.getId().toString()))
                                            .body(result);
                                    }
                                    else {
                                        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Ruang: " + booking.getSpace().getName_space() + " " + "Max Capacity: " + booking.getSpace().getCapacity())).body(null);
                                    }
                                }
//
                                else {
                                    return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Booking already exist")).body(null);
                                }
                            }
                        }
                        else if( booking.getStartsAt().getHour() ==  booking.getEndsAt().getHour()){
                            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Booking Minimal 1 Hour")).body(null);
                        }
                        else{
                            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Invalid TIme")).body(null);
                        }
                    }
                    else {
                        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Invalid Date")).body(null);
                    }
                }
                else{
                    return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Booking time minimal 08:00 AM until 08:00 PM")).body(null);
                }
            }
            else {
                return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Invalid Date")).body(null);
            }
        }else {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Invalid Date")).body(null);
        }


    }

    /**
     * GET  /bookings -> get all the bookings.
     */
    @RequestMapping(value = "/bookings",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Booking>> getAllBookings(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Bookings");
        Page<Booking> page = bookingRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bookings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bookings -> get all by Booked By.
     */
    @RequestMapping(value = "/bookings/by-login",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Booking>> getAllBookingBy(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Bookings Booked By");
        String loginUsername = SecurityUtils.getCurrentUser().getUsername();
        Page<Booking> page = bookingRepository.findAllByBookingBy(loginUsername, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bookings/by-login");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /bookings -> get all by Status id.
     * 0 = Pending
     * 1 = Approved
     * 2 = Rejected
     */
    @RequestMapping(value = "/bookings/by-status/approved",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Booking>> getAllStatusApproved(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Bookings Booked By");
        Page<Booking> page = bookingRepository.findAllByStatus("Approved", pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bookings/by-status/approved");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/bookings/by-status/pending",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Booking>> getAllStatusPending(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Bookings Booked By");
        Page<Booking> page = bookingRepository.findAllByStatus("Pending", pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bookings/by-status/pending");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/bookings/by-status/rejected",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Booking>> getAllStatusRejected(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Bookings Booked By");
        Page<Booking> page = bookingRepository.findAllByStatus("Rejected", pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bookings/by-status/rejected");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }




    /**
     * GET  /bookings/:id -> get the "id" booking.
     */
    @RequestMapping(value = "/bookings/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Booking> getBooking(@PathVariable Long id) {
        log.debug("REST request to get Booking : {}", id);
        Booking booking = bookingRepository.findOne(id);
        return Optional.ofNullable(booking)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    /**
     * DELETE  /bookings/:id -> delete the "id" booking.
     */
    @RequestMapping(value = "/bookings/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteBooking(@PathVariable Long id) {
        log.debug("REST request to delete Booking : {}", id);
        bookingRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("booking", id.toString())).build();
    }
}


//
//    /**
//     * POST  /bookings -> Create a new booking.
//     */
//    @RequestMapping(value = "/bookings",
//        method = RequestMethod.POST,
//        produces = MediaType.APPLICATION_JSON_VALUE)
//    @Timed
//    public ResponseEntity<Booking> createBooking(@Valid @RequestBody Booking booking) throws URISyntaxException {
//        log.debug("REST request to save Booking : {}", booking);
//
//        if (booking.getId() != null) {
//            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "A new booking cannot already have an ID")).body(null);
//        }
//
//        List<Booking> result1 = bookingRepository.findByStartsAtBetweenAndSpace(booking.getStartsAt(), booking.getEndsAt(), booking.getSpace());
//        List<Booking> result2 = bookingRepository.findByEndsAtBetweenAndSpace(booking.getStartsAt(), booking.getEndsAt(), booking.getSpace());
//        List<Booking> result3 = bookingRepository.findByStartsAtLessThanEqualAndEndsAtGreaterThanEqualAndSpace(booking.getStartsAt(), booking.getEndsAt(), booking.getSpace());
//
//
//        if ((result1.size() == 0 ) && (result2.size() == 0) && (result3.size() ==0))
//        {
//            if((booking.getStartsAt().getYear() == booking.getEndsAt().getYear()) && (booking.getStartsAt().getMonth() == booking.getEndsAt().getMonth()) && (booking.getStartsAt().getDayOfMonth() == booking.getEndsAt().getDayOfMonth())) {
//                if (booking.getTitle().length() >= 3) {
//                    System.out.println("Test: "+ booking.getStartsAt().getMonth());
//                    Booking result = bookingRepository.save(booking);
//                    return ResponseEntity.created(new URI("/api/bookings/" + result.getId()))
//                        .headers(HeaderUtil.createEntityCreationAlert("booking", result.getId().toString()))
//                        .body(result);
//                } else {
//                    return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Title: Minimum 3 character")).body(null);
//                }
//            }
//            else {
//                return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "Invalid Date")).body(null);
//            }
//        }
//
//        else if((result1.size()>0) && (result2.size()>0) && (result3.size()>0)){
//            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "A new booking already use")).body(null);
//        }
//        else
//            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("booking", "idexists", "A new booking already use")).body(null);
//
//    }
