'use strict';

describe('Controller Tests', function() {

    describe('Space_category Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSpace_category;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSpace_category = jasmine.createSpy('MockSpace_category');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Space_category': MockSpace_category
            };
            createController = function() {
                $injector.get('$controller')("Space_categoryDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'spaceboostApp:space_categoryUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
