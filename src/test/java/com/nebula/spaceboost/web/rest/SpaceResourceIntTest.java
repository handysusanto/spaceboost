package com.nebula.spaceboost.web.rest;

import com.nebula.spaceboost.Application;
import com.nebula.spaceboost.domain.Space;
import com.nebula.spaceboost.repository.SpaceRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the SpaceResource REST controller.
 *
 * @see SpaceResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SpaceResourceIntTest {

    private static final String DEFAULT_NAME_SPACE = "AAAAA";
    private static final String UPDATED_NAME_SPACE = "BBBBB";
    private static final String DEFAULT_LOCATION = "AAAAA";
    private static final String UPDATED_LOCATION = "BBBBB";

    private static final Integer DEFAULT_CAPACITY = 1;
    private static final Integer UPDATED_CAPACITY = 2;
    private static final String DEFAULT_FACILITY_SPACE = "AAAAA";
    private static final String UPDATED_FACILITY_SPACE = "BBBBB";
    private static final String DEFAULT_NOTE_SPACE = "AAAAA";
    private static final String UPDATED_NOTE_SPACE = "BBBBB";

    @Inject
    private SpaceRepository spaceRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSpaceMockMvc;

    private Space space;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SpaceResource spaceResource = new SpaceResource();
        ReflectionTestUtils.setField(spaceResource, "spaceRepository", spaceRepository);
        this.restSpaceMockMvc = MockMvcBuilders.standaloneSetup(spaceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        space = new Space();
        space.setName_space(DEFAULT_NAME_SPACE);
        space.setLocation(DEFAULT_LOCATION);
        space.setCapacity(DEFAULT_CAPACITY);
        space.setFacility_space(DEFAULT_FACILITY_SPACE);
        space.setNote_space(DEFAULT_NOTE_SPACE);
    }

    @Test
    @Transactional
    public void createSpace() throws Exception {
        int databaseSizeBeforeCreate = spaceRepository.findAll().size();

        // Create the Space

        restSpaceMockMvc.perform(post("/api/spaces")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space)))
                .andExpect(status().isCreated());

        // Validate the Space in the database
        List<Space> spaces = spaceRepository.findAll();
        assertThat(spaces).hasSize(databaseSizeBeforeCreate + 1);
        Space testSpace = spaces.get(spaces.size() - 1);
        assertThat(testSpace.getName_space()).isEqualTo(DEFAULT_NAME_SPACE);
        assertThat(testSpace.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testSpace.getCapacity()).isEqualTo(DEFAULT_CAPACITY);
        assertThat(testSpace.getFacility_space()).isEqualTo(DEFAULT_FACILITY_SPACE);
        assertThat(testSpace.getNote_space()).isEqualTo(DEFAULT_NOTE_SPACE);
    }

    @Test
    @Transactional
    public void checkName_spaceIsRequired() throws Exception {
        int databaseSizeBeforeTest = spaceRepository.findAll().size();
        // set the field null
        space.setName_space(null);

        // Create the Space, which fails.

        restSpaceMockMvc.perform(post("/api/spaces")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space)))
                .andExpect(status().isBadRequest());

        List<Space> spaces = spaceRepository.findAll();
        assertThat(spaces).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLocationIsRequired() throws Exception {
        int databaseSizeBeforeTest = spaceRepository.findAll().size();
        // set the field null
        space.setLocation(null);

        // Create the Space, which fails.

        restSpaceMockMvc.perform(post("/api/spaces")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space)))
                .andExpect(status().isBadRequest());

        List<Space> spaces = spaceRepository.findAll();
        assertThat(spaces).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCapacityIsRequired() throws Exception {
        int databaseSizeBeforeTest = spaceRepository.findAll().size();
        // set the field null
        space.setCapacity(null);

        // Create the Space, which fails.

        restSpaceMockMvc.perform(post("/api/spaces")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space)))
                .andExpect(status().isBadRequest());

        List<Space> spaces = spaceRepository.findAll();
        assertThat(spaces).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFacility_spaceIsRequired() throws Exception {
        int databaseSizeBeforeTest = spaceRepository.findAll().size();
        // set the field null
        space.setFacility_space(null);

        // Create the Space, which fails.

        restSpaceMockMvc.perform(post("/api/spaces")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space)))
                .andExpect(status().isBadRequest());

        List<Space> spaces = spaceRepository.findAll();
        assertThat(spaces).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpaces() throws Exception {
        // Initialize the database
        spaceRepository.saveAndFlush(space);

        // Get all the spaces
        restSpaceMockMvc.perform(get("/api/spaces?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(space.getId().intValue())))
                .andExpect(jsonPath("$.[*].name_space").value(hasItem(DEFAULT_NAME_SPACE.toString())))
                .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
                .andExpect(jsonPath("$.[*].capacity").value(hasItem(DEFAULT_CAPACITY)))
                .andExpect(jsonPath("$.[*].facility_space").value(hasItem(DEFAULT_FACILITY_SPACE.toString())))
                .andExpect(jsonPath("$.[*].note_space").value(hasItem(DEFAULT_NOTE_SPACE.toString())));
    }

    @Test
    @Transactional
    public void getSpace() throws Exception {
        // Initialize the database
        spaceRepository.saveAndFlush(space);

        // Get the space
        restSpaceMockMvc.perform(get("/api/spaces/{id}", space.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(space.getId().intValue()))
            .andExpect(jsonPath("$.name_space").value(DEFAULT_NAME_SPACE.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.capacity").value(DEFAULT_CAPACITY))
            .andExpect(jsonPath("$.facility_space").value(DEFAULT_FACILITY_SPACE.toString()))
            .andExpect(jsonPath("$.note_space").value(DEFAULT_NOTE_SPACE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSpace() throws Exception {
        // Get the space
        restSpaceMockMvc.perform(get("/api/spaces/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpace() throws Exception {
        // Initialize the database
        spaceRepository.saveAndFlush(space);

		int databaseSizeBeforeUpdate = spaceRepository.findAll().size();

        // Update the space
        space.setName_space(UPDATED_NAME_SPACE);
        space.setLocation(UPDATED_LOCATION);
        space.setCapacity(UPDATED_CAPACITY);
        space.setFacility_space(UPDATED_FACILITY_SPACE);
        space.setNote_space(UPDATED_NOTE_SPACE);

        restSpaceMockMvc.perform(put("/api/spaces")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space)))
                .andExpect(status().isOk());

        // Validate the Space in the database
        List<Space> spaces = spaceRepository.findAll();
        assertThat(spaces).hasSize(databaseSizeBeforeUpdate);
        Space testSpace = spaces.get(spaces.size() - 1);
        assertThat(testSpace.getName_space()).isEqualTo(UPDATED_NAME_SPACE);
        assertThat(testSpace.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testSpace.getCapacity()).isEqualTo(UPDATED_CAPACITY);
        assertThat(testSpace.getFacility_space()).isEqualTo(UPDATED_FACILITY_SPACE);
        assertThat(testSpace.getNote_space()).isEqualTo(UPDATED_NOTE_SPACE);
    }

    @Test
    @Transactional
    public void deleteSpace() throws Exception {
        // Initialize the database
        spaceRepository.saveAndFlush(space);

		int databaseSizeBeforeDelete = spaceRepository.findAll().size();

        // Get the space
        restSpaceMockMvc.perform(delete("/api/spaces/{id}", space.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Space> spaces = spaceRepository.findAll();
        assertThat(spaces).hasSize(databaseSizeBeforeDelete - 1);
    }
}
