package com.nebula.spaceboost.web.rest;

import com.nebula.spaceboost.Application;
import com.nebula.spaceboost.domain.Space_category;
import com.nebula.spaceboost.repository.Space_categoryRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the Space_categoryResource REST controller.
 *
 * @see Space_categoryResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class Space_categoryResourceIntTest {

    private static final String DEFAULT_SPACE_CATEGORY = "AAAAA";
    private static final String UPDATED_SPACE_CATEGORY = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    @Inject
    private Space_categoryRepository space_categoryRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSpace_categoryMockMvc;

    private Space_category space_category;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Space_categoryResource space_categoryResource = new Space_categoryResource();
        ReflectionTestUtils.setField(space_categoryResource, "space_categoryRepository", space_categoryRepository);
        this.restSpace_categoryMockMvc = MockMvcBuilders.standaloneSetup(space_categoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        space_category = new Space_category();
        space_category.setSpace_category(DEFAULT_SPACE_CATEGORY);
        space_category.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createSpace_category() throws Exception {
        int databaseSizeBeforeCreate = space_categoryRepository.findAll().size();

        // Create the Space_category

        restSpace_categoryMockMvc.perform(post("/api/space_categorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space_category)))
                .andExpect(status().isCreated());

        // Validate the Space_category in the database
        List<Space_category> space_categorys = space_categoryRepository.findAll();
        assertThat(space_categorys).hasSize(databaseSizeBeforeCreate + 1);
        Space_category testSpace_category = space_categorys.get(space_categorys.size() - 1);
        assertThat(testSpace_category.getSpace_category()).isEqualTo(DEFAULT_SPACE_CATEGORY);
        assertThat(testSpace_category.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void checkSpace_categoryIsRequired() throws Exception {
        int databaseSizeBeforeTest = space_categoryRepository.findAll().size();
        // set the field null
        space_category.setSpace_category(null);

        // Create the Space_category, which fails.

        restSpace_categoryMockMvc.perform(post("/api/space_categorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space_category)))
                .andExpect(status().isBadRequest());

        List<Space_category> space_categorys = space_categoryRepository.findAll();
        assertThat(space_categorys).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = space_categoryRepository.findAll().size();
        // set the field null
        space_category.setDescription(null);

        // Create the Space_category, which fails.

        restSpace_categoryMockMvc.perform(post("/api/space_categorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space_category)))
                .andExpect(status().isBadRequest());

        List<Space_category> space_categorys = space_categoryRepository.findAll();
        assertThat(space_categorys).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSpace_categorys() throws Exception {
        // Initialize the database
        space_categoryRepository.saveAndFlush(space_category);

        // Get all the space_categorys
        restSpace_categoryMockMvc.perform(get("/api/space_categorys?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(space_category.getId().intValue())))
                .andExpect(jsonPath("$.[*].space_category").value(hasItem(DEFAULT_SPACE_CATEGORY.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getSpace_category() throws Exception {
        // Initialize the database
        space_categoryRepository.saveAndFlush(space_category);

        // Get the space_category
        restSpace_categoryMockMvc.perform(get("/api/space_categorys/{id}", space_category.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(space_category.getId().intValue()))
            .andExpect(jsonPath("$.space_category").value(DEFAULT_SPACE_CATEGORY.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSpace_category() throws Exception {
        // Get the space_category
        restSpace_categoryMockMvc.perform(get("/api/space_categorys/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSpace_category() throws Exception {
        // Initialize the database
        space_categoryRepository.saveAndFlush(space_category);

		int databaseSizeBeforeUpdate = space_categoryRepository.findAll().size();

        // Update the space_category
        space_category.setSpace_category(UPDATED_SPACE_CATEGORY);
        space_category.setDescription(UPDATED_DESCRIPTION);

        restSpace_categoryMockMvc.perform(put("/api/space_categorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(space_category)))
                .andExpect(status().isOk());

        // Validate the Space_category in the database
        List<Space_category> space_categorys = space_categoryRepository.findAll();
        assertThat(space_categorys).hasSize(databaseSizeBeforeUpdate);
        Space_category testSpace_category = space_categorys.get(space_categorys.size() - 1);
        assertThat(testSpace_category.getSpace_category()).isEqualTo(UPDATED_SPACE_CATEGORY);
        assertThat(testSpace_category.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deleteSpace_category() throws Exception {
        // Initialize the database
        space_categoryRepository.saveAndFlush(space_category);

		int databaseSizeBeforeDelete = space_categoryRepository.findAll().size();

        // Get the space_category
        restSpace_categoryMockMvc.perform(delete("/api/space_categorys/{id}", space_category.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Space_category> space_categorys = space_categoryRepository.findAll();
        assertThat(space_categorys).hasSize(databaseSizeBeforeDelete - 1);
    }
}
